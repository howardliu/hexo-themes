"use strict";

// ---- Pulse Module ------------------------------------------------------------------------------

define("pulse", ["jquery", "jsCookie"], function($, jsCookie) {
	var $body	= $(".glow > .blob");
	var speed	= 100;
	var time	= 0;
	var timerId	= null;

	$.cookie.json = true;
	var cookie = $.cookie('settings');
	var settings = (typeof cookie !== "undefined") ? cookie : {
		pulseEnabled:	true,
		bgColour:		"#0c9c0e"	// green
	};

	// https://github.com/danro/jquery-easing/blob/master/jquery.easing.js
	// t: current time, b: start value, c: end value, d: total time
	var easeInOutBack = function (t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
	};

	var roundy = function(num) {
		return Math.ceil(num * 100) / 100;
	};

	var pulse = function() {
		time += speed;
		if (time > 2000 || time < 0) {
			speed = -speed;
			time  += speed;
		}
		var size = easeInOutBack(time, 100, 175, 2500);

		// grow a background image - requires a 30Kb image
		var bgSize = 500 *  size / 250;
		var bgPos  = 300 - (bgSize / 2);
		$body.css("background-size",	bgSize + "px");
		$body.css("width",     			bgSize + "px");
		$body.css("height",     		bgSize + "px");
		$body.css("top",     			bgPos  + "px");
		$body.css("left",     			bgPos  + "px");
		$body.css("background-color",	settings.bgColour);
	};

	var setTicks = function() {
		$(".settings [data-pulse-colour]").each(function() {
			if ($(this).attr("data-pulse-colour") === settings.bgColour) {
				$(this).find("i").removeClass("fa-none").addClass("fa-check");
			} else {
				$(this).find("i").removeClass("fa-check").addClass("fa-none");
			}
		});
		$(".settings [data-pulse-toggle]").each(function() {
			if (settings.pulseEnabled) {
				$(this).find("i").removeClass("fa-none").addClass("fa-check");
			} else {
				$(this).find("i").removeClass("fa-check").addClass("fa-none");
			}
		});
	};

	var setEnabled = function(enabled) {
		settings.pulseEnabled = enabled;
		setTicks();

		if (timerId !== null) {
			clearInterval(timerId);
			timerId = null;
		}

		if (enabled) {
			timerId = setInterval(pulse, 100);
		} else {
			// this freezes the pulse at full growth
			//speed	= 100;
			//time	= 2000;
			//pulse();
		}
	};

	var setBgColour = function(bgColour) {
		settings.bgColour = bgColour;
		setTicks();

		if (!settings.pulseEnabled)
			pulse();
	};

	var saveSettings = function() {
		$.cookie('settings', settings, { expires: 3, path: '/' });
	};

	return {
		init: function() {
			$(document).ready(function() {
				$(".settings [data-pulse-colour]").click(function(event) {
					event.preventDefault();
					setBgColour($(this).attr("data-pulse-colour"))
					saveSettings();
				});

				$(".settings [data-pulse-toggle]").click(function(event) {
					event.preventDefault();
					setEnabled(!settings.pulseEnabled);
					saveSettings();
				});

				setTicks();
				setEnabled(settings.pulseEnabled);

				if (!settings.pulseEnabled)
					pulse();
			});
		},

		setBgColour:	setBgColour,
		setEnabled:		setEnabled,
		saveSettings:	saveSettings
	}
});



// ---- CSS Not For IE! ---------------------------------------------------------------------------

define("cssNotForIe", ["jquery"], function($) {

	// poor Internet Explorer 11 can't cope with all the text-shadows in the code sections!

	var ua		= window.navigator.userAgent;
	var msie	= ua.indexOf('MSIE ') > 0;
	var trident = ua.indexOf('Trident/') > 0;
	var edge	= ua.indexOf('Edge/') > 0;
	if (msie || trident || edge) {
		$(".notForIe").removeClass("notForIe").addClass("forIe");
	}
});



// ---- Select Code -------------------------------------------------------------------------------

define("selectCode", ["jquery"], function($) {

	// http://stackoverflow.com/questions/985272/selecting-text-in-an-element-akin-to-highlighting-with-your-mouse
	function selectText(text) {
		var doc = document, range, selection;
		if (doc.body.createTextRange) {
			range = document.body.createTextRange();
			range.moveToElementText(text);
			range.select();
		} else if (window.getSelection) {
			selection = window.getSelection();
			range = document.createRange();
			range.selectNodeContents(text);
			selection.removeAllRanges();
			selection.addRange(range);
		}
	}

	$(".selectCode").click(function(e) {
		selectText($(e.target).next()[0]);
	});
});



// ---- Blend -------------------------------------------------------------------------------------

define("blend", ["jquery"], function($) {

	// don't wait until document ready, do it as soon as!
	$(".blendChildren").children("h2, h3, h4, p, li, dt, dd").each(function() {
		$(this).contents().wrapAll("<span class='blend'/>")
	});

});



// ---- OffCanvas Menu ----------------------------------------------------------------------------

define("offCanvasMenu", ["jquery", "bootstrap"], function($, bs) {

	$(document).ready(function () {
		$('[data-toggle="offcanvas"]').click(function() {
			$('.row-offcanvas').toggleClass('active');
		});
	});

});



// ---- Unscramble --------------------------------------------------------------------------------

define("unscramble", [], function() {

	var randomise = function (strLength) {
		var text = "";
    	var possible = "!#$%()*+-/:=?@~ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%()*+-/:=?@~";
    	for( var i=0; i < strLength; i++ )
    	    text += possible.charAt(Math.floor(Math.random() * possible.length));
    	return text;
	}

	var countdown = function (element, secret, cnt) {
		var text = secret.substr(0, cnt) + randomise(secret.length - cnt);
		// don't replace <i> icon </i> elements!
		if (element.children.length === 0)
			element.innerHTML = text;
		element.href = "mailto:" + text;
		if (cnt++ <= secret.length)
			setTimeout(function() {
				countdown(element, secret, cnt);
			}, 100);
	};

	return function(cssSelector) {
		var elements = document.querySelectorAll(cssSelector);
		for (var i = 0; i < elements.length; i++) {
			var element = elements[i];
			var secret = element.getAttribute("data-unscramble").split("").reverse().join("");
			countdown(element, secret, 0);
		}
	};
});



// ---- Script Loader Module ----------------------------------------------------------------------

define("onRevealLoadScript", ["onReveal", "loadScript", "pulse"], function (onReveal, loadScript, pulse) {
	return function(div, scriptUrl) {
		onReveal(div, function() {
			loadScript(scriptUrl);
			pulse.setEnabled(false);
		})
	};
});

define("onReveal", ["jquery"], function ($) {
	return function(div, func) {
		$(document).ready(function () {
			var $div = $(div);
			if ($div.length > 0 ) {

				var revealed = false;
				var divTop = $div.offset().top;
				var revealCheck = function () {
					if (!revealed && $(window).scrollTop() + $(window).height() > divTop) {
						revealed = true;
						func();
					}
				};

				$(window).scroll(revealCheck);
				revealCheck();
			}
		});
	}
});

define("loadScript", [], function () {
	return function(scriptUrl) {
		var scriptTag = document.createElement('script');
		scriptTag.type = 'text/javascript';
		scriptTag.async = true;
		scriptTag.src = scriptUrl;
		(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(scriptTag);
	}
});



// ---- GridTilt Module ---------------------------------------------------------------------------

define("gridTilt", ["jquery"], function($) {
	var grid = document.getElementsByClassName("gridtilt")[0];
	var blueprint = document.getElementsByClassName("blueprint")[0];
	var pageX, pageY, x, y, dx, dy;
	var maxTiltAngle = 30;

	// use jQuery for cross browser eventing (required?) vs querySelector()?
	$('[data-gridtilt=hoverPad]').on("mousemove", function(e) {
		pageX = e.pageX;
		pageY = e.pageY;

		x  = pageX - grid.offsetLeft - (grid.offsetWidth  / 2);
		y  = pageY - grid.offsetTop  - (grid.offsetHeight / 2);

		dx = x * maxTiltAngle / (-grid.offsetWidth  / 2);
		dy = y * maxTiltAngle / (-grid.offsetHeight / 2);

		blueprint.style.transform = "rotateY(" + dx + "deg) rotateX(" + dy + "deg)";
	})
});



// ---- Javascript Cookie -------------------------------------------------------------------------

/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
define("jsCookie", ["jquery"], function($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (value !== undefined && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setTime(+t + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {};

		// To prevent the for loop in the first place assign an empty array
		// in case there are no cookies at all. Also prevents odd result when
		// calling $.cookie().
		var cookies = document.cookie ? document.cookie.split('; ') : [];

		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = parts.join('=');

			if (key && key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) === undefined) {
			return false;
		}

		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

});



// ---- AnchorJS Module ---------------------------------------------------------------------------

/**
 * AnchorJS - v3.2.1 - 2016-07-18
 * https://github.com/bryanbraun/anchorjs
 * Copyright (c) 2016 Bryan Braun; Licensed MIT
 */
!function(A,e){"use strict";"function"==typeof define&&define.amd?define("anchorJs", [],e):"object"==typeof module&&module.exports?module.exports=e():(A.AnchorJS=e(),A.anchors=new A.AnchorJS)}(this,function(){"use strict";function A(A){function e(A){A.icon=A.hasOwnProperty("icon")?A.icon:"",A.visible=A.hasOwnProperty("visible")?A.visible:"hover",A.placement=A.hasOwnProperty("placement")?A.placement:"right",A.class=A.hasOwnProperty("class")?A.class:"",A.truncate=A.hasOwnProperty("truncate")?Math.floor(A.truncate):64}function t(A){var e;if("string"==typeof A||A instanceof String)e=[].slice.call(document.querySelectorAll(A));else{if(!(Array.isArray(A)||A instanceof NodeList))throw new Error("The selector provided to AnchorJS was invalid.");e=[].slice.call(A)}return e}function n(){if(null===document.head.querySelector("style.anchorjs")){var A,e=document.createElement("style"),t=" .anchorjs-link {   opacity: 0;   text-decoration: none;   -webkit-font-smoothing: antialiased;   -moz-osx-font-smoothing: grayscale; }",n=" *:hover > .anchorjs-link, .anchorjs-link:focus  {   opacity: 1; }",o=' @font-face {   font-family: "anchorjs-icons";   font-style: normal;   font-weight: normal;   src: url(data:application/x-font-ttf;charset=utf-8;base64,AAEAAAALAIAAAwAwT1MvMg8SBTUAAAC8AAAAYGNtYXAWi9QdAAABHAAAAFRnYXNwAAAAEAAAAXAAAAAIZ2x5Zgq29TcAAAF4AAABNGhlYWQEZM3pAAACrAAAADZoaGVhBhUDxgAAAuQAAAAkaG10eASAADEAAAMIAAAAFGxvY2EAKACuAAADHAAAAAxtYXhwAAgAVwAAAygAAAAgbmFtZQ5yJ3cAAANIAAAB2nBvc3QAAwAAAAAFJAAAACAAAwJAAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADpywPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAOAAAAAoACAACAAIAAQAg6cv//f//AAAAAAAg6cv//f//AAH/4xY5AAMAAQAAAAAAAAAAAAAAAQAB//8ADwABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAACADEARAJTAsAAKwBUAAABIiYnJjQ/AT4BMzIWFxYUDwEGIicmND8BNjQnLgEjIgYPAQYUFxYUBw4BIwciJicmND8BNjIXFhQPAQYUFx4BMzI2PwE2NCcmNDc2MhcWFA8BDgEjARQGDAUtLXoWOR8fORYtLTgKGwoKCjgaGg0gEhIgDXoaGgkJBQwHdR85Fi0tOAobCgoKOBoaDSASEiANehoaCQkKGwotLXoWOR8BMwUFLYEuehYXFxYugC44CQkKGwo4GkoaDQ0NDXoaShoKGwoFBe8XFi6ALjgJCQobCjgaShoNDQ0NehpKGgobCgoKLYEuehYXAAEAAAABAACiToc1Xw889QALBAAAAAAA0XnFFgAAAADRecUWAAAAAAJTAsAAAAAIAAIAAAAAAAAAAQAAA8D/wAAABAAAAAAAAlMAAQAAAAAAAAAAAAAAAAAAAAUAAAAAAAAAAAAAAAACAAAAAoAAMQAAAAAACgAUAB4AmgABAAAABQBVAAIAAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAADgCuAAEAAAAAAAEADgAAAAEAAAAAAAIABwCfAAEAAAAAAAMADgBLAAEAAAAAAAQADgC0AAEAAAAAAAUACwAqAAEAAAAAAAYADgB1AAEAAAAAAAoAGgDeAAMAAQQJAAEAHAAOAAMAAQQJAAIADgCmAAMAAQQJAAMAHABZAAMAAQQJAAQAHADCAAMAAQQJAAUAFgA1AAMAAQQJAAYAHACDAAMAAQQJAAoANAD4YW5jaG9yanMtaWNvbnMAYQBuAGMAaABvAHIAagBzAC0AaQBjAG8AbgBzVmVyc2lvbiAxLjAAVgBlAHIAcwBpAG8AbgAgADEALgAwYW5jaG9yanMtaWNvbnMAYQBuAGMAaABvAHIAagBzAC0AaQBjAG8AbgBzYW5jaG9yanMtaWNvbnMAYQBuAGMAaABvAHIAagBzAC0AaQBjAG8AbgBzUmVndWxhcgBSAGUAZwB1AGwAYQByYW5jaG9yanMtaWNvbnMAYQBuAGMAaABvAHIAagBzAC0AaQBjAG8AbgBzRm9udCBnZW5lcmF0ZWQgYnkgSWNvTW9vbi4ARgBvAG4AdAAgAGcAZQBuAGUAcgBhAHQAZQBkACAAYgB5ACAASQBjAG8ATQBvAG8AbgAuAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==) format("truetype"); }',i=" [data-anchorjs-icon]::after {   content: attr(data-anchorjs-icon); }";e.className="anchorjs",e.appendChild(document.createTextNode("")),A=document.head.querySelector('[rel="stylesheet"], style'),void 0===A?document.head.appendChild(e):document.head.insertBefore(e,A),e.sheet.insertRule(t,e.sheet.cssRules.length),e.sheet.insertRule(n,e.sheet.cssRules.length),e.sheet.insertRule(i,e.sheet.cssRules.length),e.sheet.insertRule(o,e.sheet.cssRules.length)}}this.options=A||{},this.elements=[],e(this.options),this.isTouchDevice=function(){return!!("ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch)},this.add=function(A){var o,i,s,a,r,c,l,h,u,g,B,f,d=[];if(e(this.options),f=this.options.visible,"touch"===f&&(f=this.isTouchDevice()?"always":"hover"),A||(A="h1, h2, h3, h4, h5, h6"),o=t(A),0===o.length)return!1;for(n(),i=document.querySelectorAll("[id]"),s=[].map.call(i,function(A){return A.id}),r=0;r<o.length;r++)if(this.hasAnchorJSLink(o[r]))d.push(r);else{if(o[r].hasAttribute("id"))a=o[r].getAttribute("id");else{h=this.urlify(o[r].textContent),u=h,l=0;do void 0!==c&&(u=h+"-"+l),c=s.indexOf(u),l+=1;while(-1!==c);c=void 0,s.push(u),o[r].setAttribute("id",u),a=u}g=a.replace(/-/g," "),B=document.createElement("a"),B.className="anchorjs-link "+this.options.class,B.href="#"+a,B.setAttribute("aria-label","Anchor link for: "+g),B.setAttribute("data-anchorjs-icon",this.options.icon),"always"===f&&(B.style.opacity="1"),""===this.options.icon&&(B.style.fontFamily="anchorjs-icons",B.style.fontStyle="normal",B.style.fontVariant="normal",B.style.fontWeight="normal",B.style.lineHeight=1,"left"===this.options.placement&&(B.style.lineHeight="inherit")),"left"===this.options.placement?(B.style.position="absolute",B.style.marginLeft="-1em",B.style.paddingRight="0.5em",o[r].insertBefore(B,o[r].firstChild)):(B.style.paddingLeft="0.375em",o[r].appendChild(B))}for(r=0;r<d.length;r++)o.splice(d[r]-r,1);return this.elements=this.elements.concat(o),this},this.remove=function(A){for(var e,n,o=t(A),i=0;i<o.length;i++)n=o[i].querySelector(".anchorjs-link"),n&&(e=this.elements.indexOf(o[i]),-1!==e&&this.elements.splice(e,1),o[i].removeChild(n));return this},this.removeAll=function(){this.remove(this.elements)},this.urlify=function(A){var t,n=/[& +$,:;=?@"#{}|^~[`%!'\]\.\/\(\)\*\\]/g;return this.options.truncate||e(this.options),t=A.trim().replace(/\'/gi,"").replace(n,"-").replace(/-{2,}/g,"-").substring(0,this.options.truncate).replace(/^-+|-+$/gm,"").toLowerCase()},this.hasAnchorJSLink=function(A){var e=A.firstChild&&(" "+A.firstChild.className+" ").indexOf(" anchorjs-link ")>-1,t=A.lastChild&&(" "+A.lastChild.className+" ").indexOf(" anchorjs-link ")>-1;return e||t||!1}}return A});

define("runAnchorJs", ["anchorJs"], function(anchorJs) {
	var anchors = new anchorJs();
	return function(selector) {
		anchors.options.visible   = "touch";
		anchors.options.placement = "right";
		anchors.add(selector);
		console.info(selector)
	};
});
